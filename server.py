#!/usr/bin/env python2
import sys, time, threading, os, socket
import \
    config,     \
    colorize,   \
    music,      \
    networking, \
    system,     \
    timings     \

bar_counter = 0
def getStatusBar():
    global bar_counter
    default_colorize = colorize.fg('Light gray') + colorize.bg('default')

    # Music
    music_bar = '%s%s%s' % (
        default_colorize, music.recv(), default_colorize
    )

    # Networking
    networking_fails = networking.networkFails()
    networking_color = colorize.fg('Red') if networking_fails else default_colorize
    networking_bar = '%s[Network errors detected]%s' % (
        networking_color, default_colorize
    )

    # Timings
    time_tracker_status = timings.timeStatus()
    if not time_tracker_status:
        time_tracker_color = colorize.fg('Magenta')
    elif time_tracker_status <= 0:
        time_tracker_color = colorize.fg('Red')
    else:
        time_tracker_color = default_colorize

    if bar_counter // 30:
        current_time = timings.dateAsWords()
    else:
        current_time = timings.dateAsNumbers()

    timing_bar = '%s%s%s | %s' % (
        time_tracker_color, timings.timeStatusProcessed(), default_colorize,
        current_time
    )

    # Power
    (percent, secsleft, power_plugged) = system.power()
    power_plugged_icon = ''
    if not power_plugged:
        power_plugged_icon  = ''
        power_plugged_icon += colorize.fg('Light red')
        power_plugged_icon += '[!]'
        power_plugged_icon += default_colorize

    if 0 < secsleft <= 5 * 60:
        power_color = colorize.fgbg('Black', 'Red') if bar_counter % 2 else colorize.fgbg('Red', 'Black')
    elif percent > 75:
        power_color = default_colorize #colorize.fg('Light green')
    elif percent > 50:
        power_color = colorize.fg('Light yellow')
    elif percent > 25:
        power_color = colorize.fg('Light red')

    power_bar = '%s%sBattery:%.01f%%%s' % (
        power_plugged_icon,
        power_color, percent, default_colorize
    )

    # Memory
    mem = system.mem()
    if mem < 25:
        mem_color = default_colorize #colorize.fg('Light green')
    elif mem < 50:
        mem_color = colorize.fg('Light yellow')
    elif mem < 75:
        mem_color = colorize.fg('Light red')
    else:
        mem_color = colorize.fg('Red')

    mem_bar = '%sMEM:%2.0f%%%s' % (
        mem_color, mem, default_colorize
    )

    # CPU
    cpu = system.cpu()
    if cpu < 25:
        cpu_color = default_colorize #colorize.fg('Light green')
    elif cpu < 50:
        cpu_color = colorize.fg('Light yellow')
    elif cpu < 75:
        cpu_color = colorize.fg('Light red')
    else:
        cpu_color = colorize.fg('Red')

    cpu_bar = '%sCPU:%2.0f%%%s' % (
        cpu_color, cpu, default_colorize
    )

    # load average
    la = ' '.join( map(lambda x: '%.02f' % x, system.la()) )
    la_bar = '%s%s%s' % (
        default_colorize, la, default_colorize
    )

    bar_counter += 1
    bar_counter %= 60

    status_bar = '%s %s %s %s %s %s\n' % (music_bar, timing_bar,  power_bar, mem_bar, cpu_bar, la_bar)
    if networking_fails:
        status_bar = '%s%s' % (networking_bar, status_bar)
    return status_bar

def main():
    threads = []
    for m in [music, networking, system, timings]:
        th = threading.Thread(target = m.run)
        th.daemon = True
        th.start()
        threads.append(th)

    address = config.SOCKET_PATH

    try:
        os.unlink(address)
    except OSError:
        if os.path.exists(address):
            raise
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.bind(address)
    sock.listen(1)

    while True:
        conn, client = sock.accept()
        try:
            while True:
                command = conn.recv(8).upper()
                if command == 'GET':
                    conn.sendall(getStatusBar())
                elif command == 'EXIT':
                    conn.close()
                    sock.close()
                    os.unlink(address)
                    sys.exit(0)
                else:
                    break
        finally:
            conn.close()
    ##

if __name__ == '__main__':
    main()
