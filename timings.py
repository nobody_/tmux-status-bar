import sys, time, config
from dateutil import parser
counter      = 0
time_tracker = None

def dateAsNumbers():
    return time.strftime('%H:%M %d/%m/%Y', time.gmtime())

def dateAsWords():
    return time.strftime('%I:%M %p %a %d %b %Y', time.gmtime())

def timeStatus():
    global counter, time_tracker

    if not time_tracker:
        with open(config.TRACKER_PATH, 'r') as f:
            time_tracker = f.read().strip()

    if not time_tracker:
        return False

    utracker = int(time.mktime(parser.parse(time_tracker).timetuple()))
    unow     = int(time.time())
    uleft    = (utracker + (8 * 3600) + (20 * 60)) - unow ## 8 hours working day + 20 minutes for everyday shit

    return uleft

def timeStatusProcessed():
    left = timeStatus()

    if left == False:
        return 'Check time tracker!'

    if left <= 0:
        return 'Run, you fool'

    hours   = left // 3600
    minutes = (left // 60) % 60

    return '%02u:%02u' % (hours, minutes)

def run():
    global counter, time_tracker
    while True:
        if counter == 30:
            counter      = 0
            time_tracker = None
        else:
            counter += 1
        time.sleep(1)
