import psutil, time

def networkFails():
    (
        bytes_sent,
        bytes_recv,
        packets_sent,
        packets_recv,
        errin,
        errout,
        dropin,
        dropout
    ) = psutil.net_io_counters()

    return bool(errin & errout & dropin & dropout)

def run():
    while True:
        time.sleep(3600)
