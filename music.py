import sys, time, subprocess, os

max_length    = 16
counter       = 0
result        = None

def recv():
    global result
    #while True:
    #    if not result:
    #        time.sleep(1)
    #    break
    return result

def query_mocp(what):
    process = subprocess.Popen(['mocp', '-Q', '%' + what], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds = True)
    output , err = process.communicate()
    if process.returncode != 0:
        return None
    return output

def get_track():
    ## mocp -Q '%artist - %song'
    artist = query_mocp('artist')
    song   = query_mocp('song')
    path   = query_mocp('file')

    if not path:
        return 'no music'

    if (not artist) or         \
       (not song) or           \
       (not artist.strip()) or \
       (not song.strip()):
        track = os.path.splitext(os.path.basename(path))[0]
        return track

    artist = artist.strip()
    song   = song.strip()

    track = '%s - %s' % (artist, song)
    return track

def get_portion(track):
    global max_length, counter
    length = len(track)

    if length <= max_length:
        return track

    a = counter
    b = (counter + max_length) % (length + 1)
    if a + max_length > length:
        portion = track[a:length]
        portion += track[:(max_length - len(portion))]
    else:
        portion = track[a:b]

    if a >= length:
        counter = 0
    counter += 1
    return portion

def run():
    global result, counter

    current_track = None
    request_time  = 0

    while True:
        unow = int(time.mktime(time.gmtime()))
        if (not current_track) or \
           (request_time == 0) or \
           (unow > request_time + 10):
            current_track = get_track() + '   '
            request_time  = unow
            status        = query_mocp('state')

        if not status:
            status_icon = '[@]'
        elif 'PLAY' in status:
            status_icon = '#np'
        elif 'PAUSE' in status:
            status_icon = '[~]'
        else:
            status_icon = '[?]'

        portion = get_portion(current_track)
        result  = '%s %s' % (status_icon, portion)
        time.sleep(1)
