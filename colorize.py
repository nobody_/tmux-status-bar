#for i in {0..255}; do
#    printf "\x1b[38;5;${i}mcolour${i}\x1b[0m\n"
#done

TMUX = {
    'default': {
         'Black':         0,
         'Red':           1,
         'Green':         2,
         'Yellow':        3,
         'Blue':          4,
         'Magenta':       5,
         'Cyan':          6,
         'Light gray':    7,
         'Dark gray':     8,
         'Light red':     9,
         'Light green':   10,
         'Light yellow':  11,
         'Light blue':    12,
         'Light magenta': 13,
         'Light cyan':    14,
         'White':         15
    },
    '256-colors': {
         'Black':         16,
         'Red':           160,
         'Green':         46,
         'Yellow':        190,
         'Blue':          19,
         'Magenta':       126,
         'Cyan':          85,
         'Light gray':    239,
         'Dark gray':     232,
         'Light red':     201,
         'Light green':   156,
         'Light yellow':  178,
         'Light blue':    69,
         'Light magenta': 168,
         'Light cyan':    146,
         'White':         255
    }
}

def fg(color, term = '256-colors'):
    if color == 'default':
        return '#[fg=default]'

    if not TMUX[term]:
        raise IndexError

    return '#[fg=colour%03u]' % TMUX[term][color]

def bg(color, term = '256-colors'):
    if color == 'default':
        return '#[bg=default]'

    if not TMUX[term]:
        raise IndexError

    return '#[bg=colour%03u]' % TMUX[term][color]

def fgbg(fgcolor, bgcolor, term = '256-colors'):
    if not TMUX[term]:
        raise IndexError
    return '#[fg=colour%03u, bg=colour%03u]' % (TMUX[term][fgcolor], TMUX[term][bgcolor])
