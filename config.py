import os
HOME         = os.path.expanduser("~")
TRACKER_PATH = os.path.join(HOME, '.time_tracker')
SOCKET_PATH  = '/tmp/.tmux-status-bar'
