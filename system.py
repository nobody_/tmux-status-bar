import os, psutil, time

def la():
    return os.getloadavg()

def cpu():
    return psutil.cpu_times_percent().user

def mem():
    return psutil.virtual_memory().percent

def power():
    (percent, secsleft, power_plugged) = psutil.sensors_battery()
    return (percent, secsleft, power_plugged)

def run():
    while True:
        time.sleep(3600)
