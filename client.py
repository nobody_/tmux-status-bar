#!/usr/bin/env python2
import config, sys, os, socket

sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
address = config.SOCKET_PATH
try:
    sock.connect(address)
except socket.error, msg:
    sys.stderr.write('%s\n' % (msg))
    sys.exit(1)

cmd = sys.argv[1] if len(sys.argv) > 1 else 'GET'

try:
    sock.sendall(cmd)
    if cmd == 'EXIT':
        raise
    status_bar = ''
    buf        = ''
    while buf != '\n':
        buf = sock.recv(1)
        status_bar += buf
    sys.stdout.write(status_bar)
    sys.stdout.flush()
except Exception, e:
    pass
except (socket.error, msg):
    pass
finally:
    sock.close()

